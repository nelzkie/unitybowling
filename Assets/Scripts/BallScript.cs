﻿using UnityEngine;
using System.Collections;

public class BallScript : MonoBehaviour {

    private Rigidbody rigidbody;
    public Vector3 launchVelocity;
    bool inPlay;
    public bool inplay = false;
    Vector3 ballStarPos;
    private AudioSource audioSource;
    // Use this for initialization
    void Start () {
        rigidbody = GetComponent<Rigidbody>();
      
        rigidbody.useGravity = false;
        ballStarPos = transform.position;


    }

    public void Launch(Vector3 velocity)
    {
        inplay = true;
        rigidbody.velocity = velocity;
        rigidbody.useGravity = true;
        audioSource = GetComponent<AudioSource>();
        audioSource.Play();
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    public void Reset()
    {
        
        inPlay = false;
        transform.position = ballStarPos;
        rigidbody.velocity = Vector3.zero;
        rigidbody.angularVelocity = Vector3.zero;
        rigidbody.useGravity = false;


    }
}
