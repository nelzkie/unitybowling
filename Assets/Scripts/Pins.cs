﻿using UnityEngine;
using System.Collections;

public class Pins : MonoBehaviour {
    public float standingTreshold = 3;
    Rigidbody rigidbody;
    public float distanceToRaise = 40f;
    // Use this for initialization
    void Start () {
        rigidbody = GetComponent<Rigidbody>();
    }
	
	// Update is called once per frame
	void Update () {
    
    }


    /// <summary>
    /// Detect if the pins are standing or not
    /// </summary>
    /// <returns></returns>
    public bool IsStanding()
    {

        Vector3 rotationInEuler = transform.rotation.eulerAngles;



        //if ((rotationInEuler.x < (360 - standingTreshold) && standingTreshold < rotationInEuler.x) || (rotationInEuler.z < (360 - standingTreshold) && standingTreshold < rotationInEuler.z))
        //{
        //    print(name + "  " + rotationInEuler.x + "  <  " + (360 - standingTreshold));
        //    print(name + "  " + (standingTreshold) + "  <  " + (rotationInEuler.x));
        //}

        //if (rotationInEuler.z < (360 - standingTreshold) && standingTreshold < rotationInEuler.z)
        //{
        //    print(name + "  " + rotationInEuler.z + "  <  " + (360 - standingTreshold));
        //    print(name + "  " + (standingTreshold) + "  <  " + (rotationInEuler.z));
        //}
        float tiltX = Mathf.Abs(rotationInEuler.x);
        float tiltZ = Mathf.Abs(rotationInEuler.z);

        // Take note of the exclamation mark
        return !((rotationInEuler.z < (360 - standingTreshold) && standingTreshold < rotationInEuler.z) || (rotationInEuler.x < (360 - standingTreshold) && standingTreshold < rotationInEuler.x));

    }



    public void Raise()
    {
        if (IsStanding())
        {
            rigidbody.useGravity = false;
            transform.Translate(new Vector3(0, distanceToRaise, 0), Space.World);
        }
    }
    public void Lower()
    {
        if (IsStanding())
        {
            print("Lower pins");
            transform.Translate(new Vector3(0, -distanceToRaise, 0), Space.World);
            //rigidbody.useGravity = true;
        }
    }
}
