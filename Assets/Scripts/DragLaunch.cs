﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(BallScript))]
public class DragLaunch : MonoBehaviour {

    Vector3 dragStart, dragEnd;
    float startTime, endTime;
    private BallScript ball;
	// Use this for initialization
	void Start () {
        ball = GetComponent<BallScript>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void DragStart()
    {
        dragStart = Input.mousePosition;
        startTime = Time.time;
    }

    public void DragEnd()
    {
        dragEnd = Input.mousePosition;
        endTime = Time.time;

        float dragDuration = endTime - startTime;


        float launchSpeedX = (dragEnd.x - dragStart.x) / dragDuration;

        float launchSpeedy = (dragEnd.y - dragStart.y) / dragDuration;

        Vector3 launchVelocity = new Vector3(launchSpeedX, 0,launchSpeedy);

        ball.Launch(launchVelocity);
     }

    public void MoveStart(float amount)
    {
        if (! ball.inplay)
        {
            ball.transform.Translate(new Vector3(amount, 0, 0));
        }
    }
}
