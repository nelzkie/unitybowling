﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PinSetter : MonoBehaviour {
    public Text pinText;
    bool ballEnteredBox;
  
    int lastStandingCount = -1;
    float lastChangeTime;
    BallScript ballScript;
    public GameObject pinSet;

    // Use this for initialization
    void Start () {

        ballScript = FindObjectOfType<BallScript>();

        RenewPins();
    }
	
	// Update is called once per frame
	void Update () {
        pinText.text = CountStading().ToString();
        CountStading();
        if (ballEnteredBox)
        {
            CheckStanding();
        }
    }

    private void CheckStanding()
    {
        int currentStanding = CountStading();
        if(currentStanding != lastStandingCount)
        {
            lastStandingCount = currentStanding;
            lastChangeTime = Time.time;
            return; 
        }

        float settleTime = 3f; // How long to wait to consider settled
        if(Time.time - lastChangeTime > settleTime) // if last change more than 3 seconds
        {
            PinsHaveSettled();
        }

    }

    private void PinsHaveSettled()
    {
        lastStandingCount = -1;
        ballEnteredBox = false;
        pinText.color = Color.green;
        ballScript.Reset(); 
    }


    public int CountStading()
    {
        int standingPins = 0;
        foreach (Pins pins in GameObject.FindObjectsOfType<Pins>())
        {
            if (pins.IsStanding())
            {
                standingPins++;
            }
        }
        return standingPins;
    }

    void OnTriggerEnter(Collider collider)
    {
        if (collider.GetComponent<BallScript>())
        {
            ballEnteredBox = true;
            pinText.color = Color.red;
        }
    }

    void OnTriggerExit(Collider collider)
    {
       
        GameObject obj = collider.gameObject;
        if (obj.GetComponentInParent<Pins>())
        {
            //print("Hello " + obj.GetComponentInParent<Pins>().gameObject.name);
            Destroy(obj.GetComponentInParent<Pins>().gameObject);
        }
    }

   public void RaisePins()
   {
      
        foreach (Pins pins in GameObject.FindObjectsOfType<Pins>())
        {
           pins.Raise(); 
        }

    }

    public void LowerPins()
    {
        foreach (Pins pins in GameObject.FindObjectsOfType<Pins>())
        {
            
            pins.Lower();
        }
    }

    public void RenewPins()
    {
        print("Renew pins");
        Instantiate(pinSet, new Vector3(0, 5, 1829), Quaternion.identity); 
    }


}
